#include "ifthenloop.h"
#include "marker.h"

int main(void)
{
  (void) ifthenloop(1, 2);
  MARK(101);

  (void) ifthenloop(2, 1);
  MARK(102);

  return 0;
}
