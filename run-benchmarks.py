import os
import sys
import subprocess
import re
import matplotlib.pyplot as plt
import numpy as np
import scipy.stats

#############################################################################
class Result:
  def __init__(self, name, size, cycles):
    self.name = name
    self.size = size
    self.cycles = cycles

#############################################################################
def load(results, folder, benchmark):
  with open("%s/%s.results" % (folder, benchmark)) as f:
    fread = f.read()
    m = re.search("size=(\d+)", fread)
    assert m, benchmark
    size = m.group(1)
    m = re.search("cycles=(\d+)", fread)
    assert m, benchmark
    cycles = m.group(1)
    results[benchmark] = Result(benchmark, float(size), float(cycles))

#############################################################################
def create_latex(benchmarks, results):
  tex = open("results.tex", "w")
  assert tex
  html = open("results.html", "w")
  assert html

  # Binary size
  tex.write("% Binary size\n")
  html.write("<!-- Binary size-->\n")
  array = []
  for benchmark in benchmarks:
    baseline     = results[benchmark]
    balanced     = results["%s-balanced"     % benchmark]
    balanced_ami = results["%s-balanced-ami" % benchmark]
    linear       = results["%s-linear"       % benchmark]
    linear_ami   = results["%s-linear-ami"   % benchmark]

    b  = balanced.size     / baseline.size
    ba = balanced_ami.size / baseline.size
    l  = linear.size       / baseline.size
    la = linear_ami.size   / baseline.size

    array.append((baseline.size, b, ba, l, la))

    tex.write("%s" % benchmark)

    html.write("<tr>")
    html.write("<td>%s</td>" % benchmark)

    # Baseline
    tex.write(" & %d" % baseline.size)
    html.write("<td>%d</td>" % baseline.size)

    # Relative size overhead
    tex.write(" & %.2f" % b)      
    tex.write(" & %.2f" % ba)     
    tex.write(" & %.2f" % l)      
    tex.write(" & %.2f" % la)     

    html.write("<td>%.2f</td>" % b)
    html.write("<td>%.2f</td>" % ba)
    html.write("<td>%.2f</td>" % l)
    html.write("<td>%.2f</td>" % la)

    tex.write(" \\\\\n");
    html.write("</tr>\n")

  tex.write("\\textbf{mean}")
  tex.write(" &")
  tex.write(" & \\textbf{%.2f}" % scipy.stats.gmean(array)[1])
  tex.write(" & \\textbf{%.2f}" % scipy.stats.gmean(array)[2])
  tex.write(" & \\textbf{%.2f}" % scipy.stats.gmean(array)[3])
  tex.write(" & \\textbf{%.2f}" % scipy.stats.gmean(array)[4])

  tex.write("\\\\\n\n")

  html.write("<tr>")
  html.write("<td>mean</td>")
  html.write("<td></td>")
  html.write("<td>%.2f</td>" % scipy.stats.gmean(array)[1])
  html.write("<td>%.2f</td>" % scipy.stats.gmean(array)[2])
  html.write("<td>%.2f</td>" % scipy.stats.gmean(array)[3])
  html.write("<td>%.2f</td>" % scipy.stats.gmean(array)[4])
  html.write("</tr>\n\n")

  # Running time
  tex.write("% Execution time\n")
  html.write("<!-- Execution time-->\n")
  array = []
  for benchmark in benchmarks:
    baseline     = results[benchmark]
    balanced     = results["%s-balanced"     % benchmark]
    balanced_ami = results["%s-balanced-ami" % benchmark]
    linear       = results["%s-linear"       % benchmark]
    linear_ami   = results["%s-linear-ami"   % benchmark]

    b  = balanced.cycles     / baseline.cycles
    ba = balanced_ami.cycles / baseline.cycles
    l  = linear.cycles       / baseline.cycles
    la = linear_ami.cycles   / baseline.cycles

    array.append((baseline.cycles, b, ba, l, la))

    tex.write("%s" % benchmark)
    html.write("<tr>")
    html.write("<td>%s</td>" % benchmark)

    # Baseline
    tex.write(" & %d" % baseline.cycles)
    html.write("<td>%d</td>" % baseline.cycles)

    # Relative cycles overhead
    tex.write(" & %.2f" % b)
    tex.write(" & %.2f" % ba)
    tex.write(" & %.2f" % l)
    tex.write(" & %.2f" % la)

    html.write("<td>%.2f</td>" % b)
    html.write("<td>%.2f</td>" % ba)
    html.write("<td>%.2f</td>" % l)
    html.write("<td>%.2f</td>" % la)

    tex.write(" \\\\\n");
    html.write("</tr>\n");

  tex.write("\\textbf{mean}")
  tex.write(" &")
  tex.write(" & \\textbf{%.2f}" % scipy.stats.gmean(array)[1])
  tex.write(" & \\textbf{%.2f}" % scipy.stats.gmean(array)[2])
  tex.write(" & \\textbf{%.2f}" % scipy.stats.gmean(array)[3])
  tex.write(" & \\textbf{%.2f}" % scipy.stats.gmean(array)[4])
  tex.write("\\\\\n\n")

  html.write("<tr>")
  html.write("<td>mean</td>")
  html.write("<td></td>")
  html.write("<td>%.2f</td>" % scipy.stats.gmean(array)[1])
  html.write("<td>%.2f</td>" % scipy.stats.gmean(array)[2])
  html.write("<td>%.2f</td>" % scipy.stats.gmean(array)[3])
  html.write("<td>%.2f</td>" % scipy.stats.gmean(array)[4])
  html.write("</tr>\n\n")

  tex.close()
  html.close()

#############################################################################
def create_csv(benchmarks, results):
  
  # Create the performance report
  with open("report.csv", "w") as csv:
    csv.write("name")
    # Baseline
    csv.write(",size")
    csv.write(",cycles")
    # Relative size overhead
    csv.write(",balanced")
    csv.write(",balanced-ami")
    csv.write(",linear")
    csv.write(",linear-ami")
    # Relative cycles overhead
    csv.write(",balanced")
    csv.write(",balanced-ami")
    csv.write(",linear")
    csv.write(",linear-ami")
    csv.write("\n")

    for benchmark in benchmarks:
      baseline     = results[benchmark]
      balanced     = results["%s-balanced"     % benchmark]
      balanced_ami = results["%s-balanced-ami" % benchmark]
      linear       = results["%s-linear"       % benchmark]
      linear_ami   = results["%s-linear-ami"   % benchmark]

      def p(v):
        return (v * 100) - 100

      csv.write("%s" % benchmark)
      # Baseline
      csv.write(",%d" % baseline.size)
      csv.write(",%d" % baseline.cycles)
      # Relative size overhead
      csv.write(",%.2f" % p(balanced.size     / baseline.size))
      csv.write(",%.2f" % p(balanced_ami.size / baseline.size))
      csv.write(",%.2f" % p(linear.size       / baseline.size))
      csv.write(",%.2f" % p(linear_ami.size   / baseline.size))
      # Relative cycles overhead
      csv.write(",%.2f" % p(balanced.cycles     / baseline.cycles))
      csv.write(",%.2f" % p(balanced_ami.cycles / baseline.cycles))
      csv.write(",%.2f" % p(linear.cycles       / baseline.cycles))
      csv.write(",%.2f" % p(linear_ami.cycles   / baseline.cycles))
      csv.write("\n")

#############################################################################
def create_barchart(benchmarks, results):

  labels        = []
  lbalanced     = []
  lbalanced_ami = []
  llinear       = []
  llinear_ami   = []

  for benchmark in benchmarks:
    baseline     = results[benchmark]
    balanced     = results["%s-balanced"     % benchmark]
    balanced_ami = results["%s-balanced-ami" % benchmark]
    linear       = results["%s-linear"       % benchmark]
    linear_ami   = results["%s-linear-ami"   % benchmark]

    labels.append(benchmark)
    lbalanced.append    ((balanced.cycles     / baseline.cycles) - 1)
    lbalanced_ami.append((balanced_ami.cycles / baseline.cycles) - 1)
    llinear.append      ((linear.cycles       / baseline.cycles) - 1)
    llinear_ami.append  ((linear_ami.cycles   / baseline.cycles) - 1)

  llinear_ami = [x + 1e-7 for x in llinear_ami]

  x = np.arange(len(labels))  # the label locations
  width = 0.21  # the width of the bars
  
  fig, ax = plt.subplots()
  r2 = ax.bar(x - 2*width, lbalanced    , width, label='balanced'    , bottom=1)
  r3 = ax.bar(x - 1*width, lbalanced_ami, width, label='balanced-ami', bottom=1)
  r4 = ax.bar(x + 0*width, llinear      , width, label='linear'      , bottom=1, linewidth=2)
  r5 = ax.bar(x + 1*width, llinear_ami  , width, label='linear-ami'  , bottom=1)
  
  # Add some text for labels, title and custom x-axis tick labels, etc.
  #ax.set_ylabel('Sizes')
  #ax.set_title('Sizes by benchmark and hardened form')
  ax.legend()
  ax.set_xticks(x, labels)
  ax.set_yticks(np.arange(0.9, 3.6, 0.10))
  ax.tick_params(labelright=True)
  ax.axhline(1, color='black', linewidth=0.8)
  
  '''
  ax.bar_label(r2, padding=3)
  ax.bar_label(r3, padding=3)
  ax.bar_label(r4, padding=3)
  ax.bar_label(r5, padding=3)
  '''
  
  fig.tight_layout()
  
  #plt.yscale("log")   
  plt.grid(True, 'both', 'y', linewidth=0.2)
  plt.show()

#############################################################################
if __name__ == "__main__":
  
  blacklist = []
  blacklist += ["__pycache__"]
  blacklist += ["test"]
  blacklist += ["twofish"]
  blacklist += ["call"]
  blacklist += ["ifcompound"]

  benchmarks = [x for x in os.listdir(".") if os.path.isdir(x)]
  benchmarks = [x for x in benchmarks if x not in blacklist]
  
  # Run the benchmarks and perform the different evaluations
  for benchmark in benchmarks:
    result = subprocess.run(["make", "-C", benchmark])
    if result.returncode != 0:
      sys.exit(result.returncode)
  
  # Load the performance results
  results = {}
  for benchmark in benchmarks:
    load(results, benchmark, benchmark)
    load(results, benchmark, "%s-balanced"     % benchmark)
    load(results, benchmark, "%s-balanced-ami" % benchmark)
    load(results, benchmark, "%s-linear"       % benchmark)
    load(results, benchmark, "%s-linear-ami"   % benchmark)

  create_csv(benchmarks, results)
  create_barchart(benchmarks, results)
  create_latex(benchmarks, results)
