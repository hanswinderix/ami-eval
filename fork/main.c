#include "fork.h"
#include "marker.h"

int main(void)
{
  fork(2, 3); MARK(101);
  fork(3, 2); MARK(102);

  return 0;
}
