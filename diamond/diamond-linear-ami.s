	.file	"diamond.c"
	.option nopic
	.attribute arch, "rv32i2p0_m2p0"
	.attribute unaligned_access, 0
	.attribute stack_align, 16
	.text
	.align	2
	.globl	diamond
	.type	diamond, @function
diamond:
 #APP
# 8 "diamond.c" 1
	mark 1
# 0 "" 2
 #NO_APP

  # a1 < a0
	li	a4,28
	li	a6,7

  a.bne a0, a1, .LA
  # a0 == a1
	li	a4,0
	li	a6,0
.LA:

  a.bge a0, a1, .LB
  # a0 < a1
	li	a4,12
	li	a6,3
.LB:

 #APP
# 24 "diamond.c" 1
	mark 2
# 0 "" 2
 #NO_APP
	li	a5,10
	beq	a1,a5,.L7
	mv	a0,a6
	ret
.L7:
	mv	a0,a4
	ret
.L5:
	.size	diamond, .-diamond
	.ident	"GCC: (GNU) 11.1.0"
