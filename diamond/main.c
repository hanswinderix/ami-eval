#include "diamond.h"
#include "marker.h"

int main(void)
{
  (void) diamond(1, 1);
  MARK(101);

  (void) diamond(1, 2);
  MARK(102);

  (void) diamond(2, 1);
  MARK(103);

  (void) diamond(2, 10);
  MARK(104);

  return 0;
}
