	.text
	.attribute	4, 16
	.attribute	5, "rv32i2p0"
	.file	"switch.c"
	.globl	switch_case                     # -- Begin function switch_case
	.p2align	2
	.type	switch_case,@function
switch_case:                            # @switch_case
# %bb.0:
	addi	sp, sp, -16
	sw	ra, 12(sp)                      # 4-byte Folded Spill
	sw	s0, 8(sp)                       # 4-byte Folded Spill
	addi	s0, sp, 16

	mv	t0, a0

	#APP
	mark	1
	#NO_APP

  li a1, 1
  a.bne t0, a1, .LBB0_4
	li	a0, 1
.LBB0_4:
  li a1, 2
  a.bne t0, a1, .LBB0_5
	li	a0, 2
.LBB0_5:
  li a1, 3
  a.bne t0, a1, .LBB0_6
	li	a0, 3
.LBB0_6:
  li a1, 4
  a.bne t0, a1, .LBB0_7
	li	a0, 4
.LBB0_7:
  li a1, 5
  a.bne t0, a1, .LBB0_8
	li	a0, 5
.LBB0_8:
  li a1, 6
  a.bne t0, a1, .LBB0_9
	li	a0, 6
.LBB0_9:
  li a1, 7
  a.bne t0, a1, .LBB0_10
	li	a0, 7
.LBB0_10:
  li a1, 8
  a.bne t0, a1, .LBB0_11
	li	a0, 8
.LBB0_11:
  li a1, 9
  a.bne t0, a1, .LBB0_12
	li	a0, 9
.LBB0_12:
  li a1, 10
  a.bne t0, a1, .LBB0_13
	li	a0, 10
.LBB0_13:
  li a1, 11
  a.bne t0, a1, .LBB0_14
	li	a0, 11
.LBB0_14:
  li a1, 12
  a.bne t0, a1, .LBB0_15
	li	a0, 12
.LBB0_15:
  li a1, 13
  a.bne t0, a1, .LBB0_16
	li	a0, 13
.LBB0_16:
  li a1, 14
  a.bne t0, a1, .LBB0_17
	li	a0, 14
.LBB0_17:
  li a1, 15
  a.bne t0, a1, .LBB0_18
	li	a0, 15
.LBB0_18:
  li a1, 16
  a.bne t0, a1, .LBB0_19
	li	a0, 16
.LBB0_19:
	#APP
	mark	2
	#NO_APP
	lw	ra, 12(sp)                      # 4-byte Folded Reload
	lw	s0, 8(sp)                       # 4-byte Folded Reload
	addi	sp, sp, 16
	ret
.Lfunc_end0:
	.size	switch_case, .Lfunc_end0-switch_case
                                        # -- End function
	.ident	"clang version 15.0.0 (git@gitlab.kuleuven.be:u0126303/llvm-project.git a3b1800863ff654d43bcfc8fc905822f341d9f92)"
	.section	".note.GNU-stack","",@progbits
