#include "switch.h"
#include "marker.h"

int main(void)
{
  (void) switch_case(0x0001); MARK(101);
  (void) switch_case(0x0002); MARK(102);
  (void) switch_case(0x0003); MARK(103);
  (void) switch_case(0x0004); MARK(104);
  (void) switch_case(0x0005); MARK(105);
  (void) switch_case(0x0006); MARK(106);
  (void) switch_case(0x0007); MARK(107);
  (void) switch_case(0x0008); MARK(108);
  (void) switch_case(0x0009); MARK(109);
  (void) switch_case(0x000a); MARK(110);
  (void) switch_case(0x000b); MARK(111);
  (void) switch_case(0x000c); MARK(112);
  (void) switch_case(0x000d); MARK(113);
  (void) switch_case(0x000e); MARK(114);
  (void) switch_case(0x000f); MARK(115);
  (void) switch_case(0x0010); MARK(116);

  /* None of the cases (there is no default) */
  (void) switch_case(0x0000); MARK(117);

  return 0;
}
