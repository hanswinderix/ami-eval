#include "mulmod16.h"
#include "marker.h"

int main(void)
{
  (void) mulmod16(0, 9114);
  MARK(101);

  (void) mulmod16(7906, 0);
  MARK(102);

  (void) mulmod16(7, 8);
  MARK(103);

  (void) mulmod16(267, 13853);
  MARK(104);

  (void) mulmod16(13853, 267);
  MARK(105);

  return 0;
}
