#include "twofish.h"
#include "marker.h"

int main(void)
{
  static uint8_t key[16] = {
    0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07,
    0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0X0F
  };

  twofish_key_schedule(key, sizeof(key)/sizeof(key[0]));
  MARK(101);

  key[0] = 0x01;
  twofish_key_schedule(key, sizeof(key)/sizeof(key[0]));
  MARK(102);

  return 0;
}
