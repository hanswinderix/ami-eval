#include "keypad.h"
#include "marker.h"

int main(void)
{
  keypad_init();

  /* With the current mockup, calling keypad_poll ten times in a row
   * covers the full range of execution times.
   */
  (void) keypad_poll(); MARK(101);
  (void) keypad_poll(); MARK(102);
  (void) keypad_poll(); MARK(103);
  (void) keypad_poll(); MARK(104);
  (void) keypad_poll(); MARK(105);
  (void) keypad_poll(); MARK(106);
  (void) keypad_poll(); MARK(107);
  (void) keypad_poll(); MARK(108);
  (void) keypad_poll(); MARK(109);
  (void) keypad_poll(); MARK(110);

  return 0;
}
