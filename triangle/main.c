#include "triangle.h"
#include "marker.h"

int main(void)
{
  (void) triangle(1, 2);
  MARK(101);

  (void) triangle(2, 1);
  MARK(102);

  return 0;
}
