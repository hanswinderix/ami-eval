#include "ifcompound.h"
#include "marker.h"

int main(void)
{
  (void) ifcompound(1, 1, 2); MARK(101);
  (void) ifcompound(1, 1, 1); MARK(102);
  (void) ifcompound(2, 1, 2); MARK(103);

  return 0;
}
