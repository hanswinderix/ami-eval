#include "ifcompound.h"
#include "marker.h"

int ifcompound(/* secret */ int a, int b, int c)
{
  int result;

  __asm__("nop");

  /* Begin of sensitive region */ MARK(1);

  /* Secret-dependent branch */
  if ( (a == b) && (b < c) )
  {
    result = 7;
  }
  else
  {
    result = 3;
  }

  /* End of sensitive region */ MARK(2);

  /* Secret-independent branch */
  if ( (b ^ c) == 0)
  {
    result *= 3;
  }

  return result;
}

