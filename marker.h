#ifndef MARKER_H
#define MARKER_H

#define MARK(id) __asm__("mark " # id);

#endif
