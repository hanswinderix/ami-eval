#include "modexp2.h"
#include "marker.h"

int main(void)
{
  (void) modexp2(10, 1);
  MARK(101);

  (void) modexp2(10, 15);
  MARK(102);

  (void) modexp2(10, 42);
  MARK(103);

  (void) modexp2(10, 142);
  MARK(104);

  return 0;
}
