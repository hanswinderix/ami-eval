#include "call.h"
#include "marker.h"

int main(void)
{
  (void) call(2, 1);
  (void) call(3, 1);

  return 0;
}
