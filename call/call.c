#include "call.h"
#include "marker.h"

static volatile int v = 0;

__attribute__((noinline)) static int foo(int x)
{
  v = x;

  return v++;
}

int call(/* secret */ int a, int b)
{
  /* Begin of sensitive region */ MARK(1);
    
  if (a == 2)
  {
    return foo(b);
  }

  /* End of sensitive region */ MARK(2);

  return 0;
}
