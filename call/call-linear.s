	.text
	.attribute	4, 16
	.attribute	5, "rv32i2p0_m2p0"
	.file	"call.c"
	.globl	call
	.p2align	2
	.type	call,@function
call:
	li	a2, 2
	#APP
	mark	1
	#NO_APP
	bne	a0, a2, .LBB0_2
	mv	a0, a1
	tail	foo
.LBB0_2:
	#APP
	mark	2
	#NO_APP
	li	a0, 0
	ret
.Lfunc_end0:
	.size	call, .Lfunc_end0-call

	.p2align	2
	.type	foo,@function
foo:
	lui	a1, %hi(v)
	sw	a0, %lo(v)(a1)
	lw	a0, %lo(v)(a1)
	addi	a2, a0, 1
	sw	a2, %lo(v)(a1)
	ret
.Lfunc_end1:
	.size	foo, .Lfunc_end1-foo

	.type	v,@object
	.section	.sbss,"aw",@nobits
	.p2align	2
v:
	.word	0
	.size	v, 4

	.ident	"clang version 15.0.0 (git@gitlab.kuleuven.be:u0126303/llvm-project.git 30c16046da33e1c74f4fcfd533c854f05403cb83)"
	.section	".note.GNU-stack","",@progbits
	.addrsig
	.addrsig_sym v
