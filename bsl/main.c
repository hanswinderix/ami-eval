#include "bsl.h"
#include "marker.h"

int main(void)
{
  static char password[BSL_PASSWORD_LENGTH] = "0123456789ABCDEF";

  BSL430_unlock_BSL(password);
  MARK(101);

  password[2] = 'X';
  BSL430_unlock_BSL(password);
  MARK(102);

  password[7] = 'X';
  password[8] = 'X';
  BSL430_unlock_BSL(password);
  MARK(103);

  password[10] = 'X';
  password[11] = 'X';
  BSL430_unlock_BSL(password);
  MARK(104);
  
  password[2] = '2';
  password[7] = '7';
  password[8] = '8';
  password[10] = 'A';
  password[11] = 'B';
  BSL430_unlock_BSL(password);
  MARK(105);

  return 0;
}
