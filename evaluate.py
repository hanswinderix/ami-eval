import ProteusVCD
import sys
import os
import re
import importlib
from pprint import pformat
from enum import Enum

debug = False

logfile = None
def log(msg):
  logfile.write("%s\n" % msg)

def trace_debug(msg):
  if debug:
    print(msg)

# Security evalutation types (SET)
#   To detect secret-dependent signal changes) 
class SET(Enum):
  DT   = 1   # Delta t only
  DV   = 2   # Delta t, and delta v (usefull e.g., when iterating over an array)
  DTV  = 3   # Delta t and v

# gSignals is a dictionary used to configure how the attacker-observable signal
# updates of different executions are compared with each other.
#
#  Key  : signal name,
#  value: tuple of 
#            1. evaluation type for balanced form,
#            2. evaluation type for linear form                                                  
gSignals = {
  'TOP.Core.pipeline.pipelineRegs_MEM_1_out_PC[31:0]'                 : (SET.DT, SET.DTV),
#  'TOP.Core.pipeline_1.memory.Lsu_dbusCtrl_currentCmd_cmd_address[31:0]': (SET.DV, SET.DV)
}

def do_include(signame):
  for k in gSignals:
    if signame == k:
      return True
  return False

# Represents a signal as a collection of deltaT/value pairs
class Signal:
  def __init__(self, name):
    self.name = name
    self.tv = {} # dict because it is easier to filter out duplicates from 
                 # signal aliases
    self.SETBalanced, self.SETLinear = gSignals[name]

  def compare(self, other, balanced):
    typ = self.SETLinear
    if balanced: typ = self.SETBalanced

    #if type == SET.N: return len(self.tv) == len(other.tv)
    if typ == SET.DT:
      return [k for k in self.tv] == [k for k in other.tv]
    elif typ == SET.DTV: 
      return self.tv == other.tv
    elif typ == SET.DV: 
      # TODO
      return len(self.tv) == len(other.tv)
    else:
      assert False, typ

# Represents a single execution of a (sensitive) region
class Execution:
  def __init__(self, beginT, endT):
    self.signals = {}
    self.beginT = beginT
    self.endT = endT

  # deltaT: t relative to the begin of the execution
  def add(self, signame, deltaT, v):
    if not signame in self.signals.keys():
      self.signals[signame] = Signal(signame)

    tv = self.signals[signame].tv
    if deltaT in tv.keys():
      assert tv[deltaT] == v, "Inconsistent value from signal alias"
    else:
      tv[deltaT] = v

  # Return true when all attacker-observable signals are equal
  def compare(self, other, balanced):
    result = True

    if (self.endT - self.beginT) != (other.endT - other.beginT):
      log("T")
      result = False

    s1 = set(self.signals.keys())
    s2 = set(other.signals.keys())

    for signame in s1.intersection(s2):
      if not self.signals[signame].compare(other.signals[signame], balanced):
        log("X %s" % signame)
        log("%s" % pformat(self.signals[signame].tv))
        log("%s" % pformat(other.signals[signame].tv))
        result = False

    for signame in s1.symmetric_difference(s2):
      log("- %s" % signame)
      result = False

    for signame in s1.intersection(s2):
      if self.signals[signame].compare(other.signals[signame], balanced):
        trace_debug("V %s" % signame)

    return result

#############################################################################
#  - A begin-end marker pair delineates a sensitive region
#    - begin_mark.WB contains the start times of the region's executions
#    - end_mark.WB contains the corresponding end times of the region's
#       executions
#
def verify_security(begin_mark, end_mark, balanced):

  return True

  # Use the new security evaluation for the in-order pipeline as well
  # This one is inferior (and fails now because of the flushcache issue)
  """
  assert len(begin_mark.WB) == len(end_mark.WB), \
             ("Non matching marks: ", begin_mark.WB, end_mark.WB)

  executions = []
  # TODO: Optimize me. It is not necessary to load all the t,v values for
  #       each execution, one pass over the signals should suffice as
  #       executions do not overlap
  for beginT, endT in zip(begin_mark.WB, end_mark.WB):
    execution = Execution(beginT, endT)
    for signal in [vcd.vcd[x] for x in vcd.vcd.signals if do_include(x)]:
      signame = signal.references[0]
      for t, v in signal.tv:
        # TODO: Make t selection more accurate
        #             (e.g., beginT should be a bit more early)
        if beginT <= t < endT:
          deltaT = t - beginT;
          execution.add(signame, deltaT, v)
    executions.append(execution)

  '''
  for e in executions:
    trace_debug(e.beginT, e.endT)
    for s in e.signals:
      trace_debug(e.signals[s].tv)
  '''

  assert len(executions) > 0
  execution0 = executions[0]
  for execution in executions[1:]:
    if not execution0.compare(execution, balanced):
      log("%d-%d != %d-%d" % (execution0.beginT, execution0.endT,
                              execution.beginT, execution.endT))
      return False

  return True

  """

#############################################################################
if __name__ == '__main__':

  # Parse command line
  assert len(sys.argv) > 1, "Benchmark name expected"
  benchmark = sys.argv[1]
  if len(sys.argv) > 2:
    debug = True

  # Load VCD file
  vcd = ProteusVCD.ProteusVCD("%s.vcd" % benchmark)
  
  with open("%s.results" % benchmark, "w") as f:
    logfile = f
    log("----------------------------------")
    log("benchmark=%s" % benchmark)

    # 1. Correctness evaluation (benchmark specific)
    # Dynamically import "<benchmark>.py" and verify correctness by comparing
    # the expected results with the actual results.
    log("Correctness evaluation:")
    basename = benchmark.split("-")[0]
    m = importlib.import_module("%s.%s" % (basename, basename))
    m.verify_correctness(benchmark, f, vcd)
    log("OK");

    # 2. Performance evaluation (generic)
    log("Performance evaluation:")
    log("size=%d" % os.path.getsize("%s.bin" % benchmark))
    log("cycles=%d" % vcd.total_cycles())
  
    # 3. Security evaluation (generic)
    # Verify for each sensitive region that all its executions are
    #  indistinguishable for adversaries
    log("Security evaluation:")
    i = 1
    result = True
    while (True):
      try:
        balanced = "balanced" in benchmark
        if not verify_security(vcd.get_mark(i), vcd.get_mark(i+1), balanced):
          log("@ mark %d" % i)
          result = False
          if (benchmark == basename):
            # The executions of the baseline (i.e., the vulnerable form) should 
            # be distinguishable
            assert not result, "Expected security violation"
          else:
            # The executions of the hardened forms should be indistinguishable
            assert result, "Unexpected security violation"
        i = i + 2
      except ProteusVCD.NoSuchMark:
        break

    assert i > 1, "No senstive regions"
    if result:
      log("OK")
