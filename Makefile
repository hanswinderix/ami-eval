PYTHON = python3

all: report.csv

report.csv:
	$(PYTHON) run-benchmarks.py

clean: 
	$(RM) report.csv

cleanresults:
	find . -name \*.results | xargs rm

realclean: 
	$(RM) report.csv
	$(MAKE) -C sharevalue clean
	$(MAKE) -C kruskal clean
	$(MAKE) -C modexp2 clean
	$(MAKE) -C triangle clean
	$(MAKE) -C mulmod16 clean
	$(MAKE) -C switch clean
	$(MAKE) -C bsl clean
	$(MAKE) -C diamond clean
	$(MAKE) -C fork clean
	$(MAKE) -C keypad clean
	$(MAKE) -C ifthenloop clean
	#(MAKE) -C ifcompound clean
	#(MAKE) -C twofish clean
	#(MAKE) -C call clean
