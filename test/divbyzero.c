int main(void)
{
  static volatile int zero;
  static volatile int one;
  static volatile int two;
  static volatile int mid;
  static volatile int max;

  zero = 0;
  one = 1;
  two = 2;
  mid = 0xFF;
  max = 0xFFFF;

  int a = 12 / zero;
  int b = 12 / two;
  int c = 36 / two;
  int d = 0xabcd / mid;
  int e = mid / mid;
  int f = max / 0xbeef;
  int g = 0xffee / max;
  int h = max / mid;

  return a + b + c + d + e + f + g + h;
}
