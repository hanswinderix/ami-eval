static volatile int test[1024];

int main(void)
{
  test[518] = test[519];
  /* Read from cache (same cache lines) */
  test[520] = test[521];
  test[522] = test[523];
  test[524] = test[525];
  test[525] = test[527];

  /* Read from memory (other cache line) */
  test[768] = test[769];

  return 0;
}
