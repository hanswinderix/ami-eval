int main(void)
{
  static volatile int zero;
  static volatile int one;
  static volatile int two;
  static volatile int mid;
  static volatile int max;

  zero = 0;
  one = 1;
  two = 2;
  mid = 0xFF;
  max = 0xFFFF;

  /* 350 ns */
  int f = max * max;
  int h = mid * mid;

  /* 340 ns */
  int a = zero * one;
  int b = one * two;
  int c = two * one;
  int d = one * max;
  int e = max * one;
  int g = zero * one;

  return a + b + c + d + e + f + g + h;
}
